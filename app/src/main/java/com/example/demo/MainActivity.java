package com.example.demo;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ViewFlipper;

public class MainActivity extends Activity {

    private Handler handler;
    private Runnable runnable;
    private static final int SPLASH_INTERVAL = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Load the fragment into frame layout by passing parameter as a fragment
     *
     * @param fragment current fragment that going to display in frame layout
     *                 <p/>
     *                 This method will check current fragment and stop rendering if current fragment is tapped again,
     *                 It also check whether fragment in backstack or not. If fragmennt in backstack it will find and load it to container without reload
     */
    private void loadFragment(Fragment fragment) {

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_main, fragment, fragment.getClass().getSimpleName());
        fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
        fragmentTransaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FragmentManager fragmentManager = getFragmentManager();
        int fragmentCount = fragmentManager.getBackStackEntryCount();
        final ViewFlipper viewFlipper=findViewById(R.id.view_flipper);
        if (fragmentCount<=0){
            viewFlipper.setDisplayedChild(0);
            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    viewFlipper.setDisplayedChild(1);
                    loadFragment(new FragmentHome());
                }
            };
            handler.postDelayed(runnable, SPLASH_INTERVAL);
        }else {
            viewFlipper.setDisplayedChild(1);
            loadFragment(new FragmentHome());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
    }
}
