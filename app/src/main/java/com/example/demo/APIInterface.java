package com.example.demo;

import com.example.demo.model.HomeResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIInterface {

    @GET("/homepage")
    Call<HomeResponse> doGetListResources();
}
