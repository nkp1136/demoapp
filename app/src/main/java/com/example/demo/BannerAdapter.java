package com.example.demo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.demo.model.Item;
import com.example.demo.model.Offer;

import java.util.ArrayList;
import java.util.List;

public class BannerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = BannerAdapter.class.getSimpleName();
    private LayoutInflater inflater;
    private List<Offer> itemList;
    private View.OnClickListener listener;
    private Context context;
    private MainActivity activity;
    private RecyclerView recyclerView;

    private final int ITEM_TYPE = 0;


    BannerAdapter(Context context, MainActivity activity) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        itemList = new ArrayList<>();
        this.context = context;
        this.activity = activity;
    }

    public void setAdapterData(List<Offer> itemList) {
        this.itemList = itemList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE) {
            return new ViewHolderItem1(inflater.inflate(R.layout.fragment_home_item_1_1, parent, false));
        } else {
            return new EmptyViewHolder(inflater.inflate(R.layout.empty_layout, parent, false));
        }
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Offer datum = itemList.get(position);

        if (datum == null) {
            return;
        }

        if (holder instanceof ViewHolderItem1) {
            ((ViewHolderItem1) holder).textViewOffer.setText(datum.getCode());
            ((ViewHolderItem1) holder).textViewOfferDesc.setText(datum.getText());

        }
    }

    @Override
    public int getItemCount() {
        if (itemList != null) {
            return itemList.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (itemList.get(position) != null) {
            return ITEM_TYPE;
        } else {
            return -1;
        }
    }

    private static class ViewHolderItem1 extends RecyclerView.ViewHolder {
        public TextView textViewOffer;
        public TextView textViewOfferDesc;

        private ViewHolderItem1(View view) {
            super(view);
            textViewOffer = (TextView) view.findViewById(R.id.text_view_offer_code);
            textViewOfferDesc = (TextView) view.findViewById(R.id.text_view_offer_desc);
        }
    }

    private static class EmptyViewHolder extends RecyclerView.ViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }
}