package com.example.demo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.demo.model.Item;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = HomeAdapter.class.getSimpleName();
    private LayoutInflater inflater;
    private List<Item> itemList;
    private View.OnClickListener listener;
    private Context context;
    private MainActivity activity;
    private RecyclerView recyclerView;

    private final int ITEM_TYPE_1 = 1;
    private final int ITEM_TYPE_2 = 2;
    private final int ITEM_TYPE_3 = 3;


    HomeAdapter(Context context, MainActivity activity) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        itemList = new ArrayList<>();
        this.context = context;
        this.activity = activity;
    }

    public void setAdapterData(List<Item> itemList) {
        this.itemList = itemList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE_1) {
            return new ViewHolderItem1(inflater.inflate(R.layout.fragment_home_item_1, parent, false));
        } else if (viewType == ITEM_TYPE_2) {
            return new ViewHolderItem2(inflater.inflate(R.layout.fragment_home_item_2, parent, false));
        } else if (viewType == ITEM_TYPE_3) {
            return new ViewHolderItem3(inflater.inflate(R.layout.fragment_home_item_3, parent, false));
        } else {
            return new EmptyViewHolder(inflater.inflate(R.layout.empty_layout, parent, false));
        }
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Item datum = itemList.get(position);

        if (datum == null) {
            return;
        }

        if (holder instanceof ViewHolderItem1) {
            Picasso.get().load(datum.getImageUrl()).into(((ViewHolderItem1) holder).offerImage);
            BannerAdapter bannerAdapter=new BannerAdapter(context,activity);
            bannerAdapter.setAdapterData(datum.getOffers());
            ((ViewHolderItem1) holder).recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayout.HORIZONTAL, false));
            ((ViewHolderItem1) holder).recyclerView.setAdapter(bannerAdapter);

            ((ViewHolderItem1) holder).textView.setText(datum.getTitle());
        } else if (holder instanceof ViewHolderItem2) {
            Picasso.get().load(datum.getImageUrl()).into(((ViewHolderItem2) holder).categoryImage);
            ((ViewHolderItem2) holder).categoryName.setText(datum.getTitle());
        } else if (holder instanceof ViewHolderItem3) {
            ((ViewHolderItem3) holder).suggestion.setText(datum.getText());
        }
    }

    @Override
    public int getItemCount() {
        if (itemList != null) {
            return itemList.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (itemList.get(position).getLayout() == 1) {
            return ITEM_TYPE_1;
        } else if (itemList.get(position).getLayout() == 2) {
            return ITEM_TYPE_2;
        } else if (itemList.get(position).getLayout() == 3) {
            return ITEM_TYPE_3;
        } else {
            return -1;
        }
    }

    private static class ViewHolderItem1 extends RecyclerView.ViewHolder {
        RecyclerView recyclerView;
        TextView textView;
        ImageView offerImage;

        private ViewHolderItem1(View view) {
            super(view);
            textView = (TextView) view.findViewById(R.id.text_view_title);
            recyclerView = (RecyclerView) view.findViewById(R.id.rv_banners);
            offerImage = view.findViewById(R.id.offer_image);
        }
    }

    private static class ViewHolderItem2 extends RecyclerView.ViewHolder {
        ImageView categoryImage;
        TextView categoryName;

        private ViewHolderItem2(View view) {
            super(view);
            categoryImage = (ImageView) view.findViewById(R.id.image_view_category);
            categoryName = (TextView) view.findViewById(R.id.text_view_category_name);
        }
    }

    private static class ViewHolderItem3 extends RecyclerView.ViewHolder {
        TextView suggestion;

        private ViewHolderItem3(View view) {
            super(view);
            suggestion = (TextView) view.findViewById(R.id.text_view_suggestion);
        }
    }


    private static class EmptyViewHolder extends RecyclerView.ViewHolder {
        EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }
}